import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../styles';

const styles = StyleSheet.create({
	container: {
		height: 33,
		backgroundColor: colors.primary,
		borderRadius: 30,
		paddingHorizontal: 20,
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		color: colors.white,
	},
	'text-outline': {
		color: colors.primary,
	},
	'button-outline': {
		backgroundColor: colors.white,
		borderWidth: 1,
		borderColor: colors.primary,
	},
});

export default styles;
