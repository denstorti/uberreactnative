import {StyleSheet} from 'react-native';
import {metrics, colors, fonts} from '../../styles';

const avatarDiameter = 68;

const styles = StyleSheet.create({
	container: {
		padding: metrics.padding,
		flexDirection: 'row',
		backgroundColor: colors.white,
		borderBottomWidth: 1,
		borderColor: colors.lighter,
	},
	avatar: {
		marginRight: metrics.padding,
		width: avatarDiameter,
		height: avatarDiameter,
		borderRadius: 40,
	},
	profileInfo: {
		flex: 1,
	},
	name: {
		fontSize: fonts.big,
		fontWeight: 'bold',
		color: colors.dark,
		marginTop: 5,
	},
	bio: {
		marginTop: 5,
		fontSize: fonts.regular,
		color: colors.regular,
	},
	buttonContainer: {
		flexDirection: 'row',
		marginTop: 10,
	},
	firstButton: {
		marginRight: 10,
	},
});

export default styles;
