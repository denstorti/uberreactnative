import React from 'react';
import {View, Image, Text} from 'react-native';
import styles from './styles';
import Button from '../Button';

const SubHeader = () => (
	<View style={styles.container}>
		<Image
			style={styles.avatar}
			source={{
				uri:
					'https://cdn.pixabay.com/photo/2012/11/21/17/02/lion-66898_1280.jpg',
			}}
		/>

		<View style={styles.profileInfo}>
			<Text style={styles.name}>Denis Storti</Text>
			<Text style={styles.bio}>
				Esta eh a minha bio. Esta eh a minha bio. Esta eh a minha bio. Esta eh a
				minha bio.
			</Text>

			<View style={styles.buttonContainer}>
				<Button style={styles.firstButton}>Message</Button>
				<Button type="outline">Follow</Button>
			</View>
		</View>
	</View>
);

export default SubHeader;
