/* Core */
import React, {Component} from 'react';

/* Presentational */
import {View} from 'react-native';
import Product from './Product';

import styles from './styles';

export default class ProductList extends Component {
	state = {
		products: [
			{
				id: 1,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy83NzE5NDEvMDM5NDM1ZmM4OGJmMmRiZTQ4MWM1YjE3MzY3OWMyM2MuanBn',
				title: 'Acne Studios',
				description: 'Andrea nappa dusty pink',
				price: 'R$50,00',
			},
			{
				id: 2,
				image:
					'https://photos.enjoei.com.br/pretinha-maria-bonita-extra-39/1200xN/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8zNTc1NDEvNzBlMDJmMzhmMjdjZGQyZGQxYzJjMGZhOGQ4N2Y3OTguanBn',
				title: 'Acne Studios',
				description: 'Lain pop sky blue',
				price: 'R$70,00',
			},
			{
				id: 3,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8xNDEzMjcvMTI4ZWU0MjYzYWYxZDQwOTY0NzViYjA2ZWUzNjkwOTYuanBn',
				title: 'Acne Studios',
				description: 'Andrea nappa dusty pink',
				price: 'R$30,00',
			},
			{
				id: 4,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8xMTMyMTA5Ny84ZDNhYzg2Mjg3ZWM1NmUyZWM5OGQ0YzI2MzQ4MWY1Ni5qcGc',
				title: 'Acne Studios',
				description: 'Lain pop sky blue',
				price: 'R$70,00',
			},
			{
				id: 5,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy83NzE5NDEvMDM5NDM1ZmM4OGJmMmRiZTQ4MWM1YjE3MzY3OWMyM2MuanBn',
				title: 'Acne Studios',
				description: 'Andrea nappa dusty pink',
				price: 'R$50,00',
			},
			{
				id: 6,
				image:
					'https://photos.enjoei.com.br/pretinha-maria-bonita-extra-39/1200xN/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8zNTc1NDEvNzBlMDJmMzhmMjdjZGQyZGQxYzJjMGZhOGQ4N2Y3OTguanBn',
				title: 'Acne Studios',
				description: 'Lain pop sky blue',
				price: 'R$70,00',
			},
			{
				id: 7,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8xNDEzMjcvMTI4ZWU0MjYzYWYxZDQwOTY0NzViYjA2ZWUzNjkwOTYuanBn',
				title: 'Acne Studios',
				description: 'Andrea nappa dusty pink',
				price: 'R$30,00',
			},
			{
				id: 8,
				image:
					'https://photos.enjoei.com.br/public/500x500/czM6Ly9waG90b3MuZW5qb2VpLmNvbS5ici9wcm9kdWN0cy8xMTMyMTA5Ny84ZDNhYzg2Mjg3ZWM1NmUyZWM5OGQ0YzI2MzQ4MWY1Ni5qcGc',
				title: 'Acne Studios',
				description: 'Lain pop sky blue',
				price: 'R$70,00',
			},
		],
	};

	render() {
		return (
			<View style={styles.container}>
				{this.state.products.map(product => (
					<Product key={product.id} product={product} />
				))}
			</View>
		);
	}
}
