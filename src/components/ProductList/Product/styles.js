import {StyleSheet, Dimensions} from 'react-native';
import {colors, fonts, metrics} from '../../../styles';

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.white,
		borderRadius: 3,
		marginBottom: metrics.padding,

		shadowColor: colors.light,
		shadowRadius: 2,
		shadowOpacity: 0.1,
		shadowOffset: {x: 0, y: 0},
		width: (width - 45) / 2,
	},
	image: {
		height: 100,
		width: '100%',
		resizeMode: 'contain',
	},
	title: {
		fontWeight: 'bold',
		color: colors.darker,
	},
	description: {
		color: colors.dark,
		fontSize: fonts.smaller,
	},
	price: {
		color: colors.light,
		fontSize: fonts.regular,
		marginTop: 5,
	},
	infoContainer: {
		padding: 10,
		alignItems: 'center',
		borderTopWidth: 1,
		borderColor: colors.lighter,
	},
	imageContainer: {
		paddingTop: metrics.padding,
		paddingHorizontal: metrics.padding,
	},
	checkIcon: {
		position: 'absolute',
		right: metrics.padding,
		top: metrics.padding,
		color: colors.primary,
		zIndex: 1,

	},
});

export default styles;
