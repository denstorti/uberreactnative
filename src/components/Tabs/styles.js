import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../styles';

const styles = StyleSheet.create({
	container: {
		height: metrics.tabBarHeight,
		flexDirection: 'row',
		backgroundColor: 'rgba(255,255,255,0.5)',
		justifyContent: 'space-around',
		alignItems: 'center',
		borderTopWidth: 1,
		borderColor: colors.lighter,
	},
	main: {
		height: 40,
		width: 40,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.primary,
		borderRadius: 50,
	},
	icon: {
		color: colors.light,
	},
	mainIcon: {
		color: 'rgba(255,255,255,0.5)',
	},
	active: {
		color: colors.primary,
	},
});

export default styles;
