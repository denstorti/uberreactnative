import React from 'react';
import {View, ScrollView, SafeAreaView} from 'react-native';

import Header from './components/Header';
import SubHeader from './components/SubHeader';
import ProductList from './components/ProductList';
import Tabs from './components/Tabs';

const App = () => {
	return (
		<SafeAreaView style={{flex: 1, backgroundColor: '#f8f8fa' }} >
			<Header />
			<ScrollView>
				<SubHeader />
				<ProductList />
			</ScrollView>
			<Tabs />
		</SafeAreaView>
	);
};

export default App;
